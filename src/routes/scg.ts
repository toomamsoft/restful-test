import * as config from '../config';
import { NextFunction, Request, Response, Router } from "express";
import { BaseRoute } from "./route";
import Places from "google-places-web";

export class SCGRoute extends BaseRoute {
    /**
     * Create the routes.
     *
     * @class SCGRoute
     * @method create
     * @static
    */
   public static create(router: Router) {
    router.post('/scg/restaurants', (req: Request, res: Response, next: NextFunction) => {
        new SCGRoute().findingallrestaurants(req, res, next);
    })
   }

    /**
     * Constructor
     *
     * @class SCGRoute
     * @constructor
    */
    constructor() {
        super();
    }

    /**
     * The home page route.
     *
     * @class SCGRoute
     * @method index
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @next {NextFunction} Execute the next method.
    */

    public findingallrestaurants(req: Request, res: Response, next: NextFunction) {
        try {
            Places.apiKey = 'AIzaSyBFW4pv9XnMmDskQkXHfjDko7oz-6fkbz4';
            Places.nearbysearch({
                location: `13.82820,100.52969`,
                Radius: `1000`,
                type : [ "cafe", "bar", "restaurant", "food", "establishment" ],
                keyword: 'restaurant',
                rankby: "distance" // See google docs for different possible values
                }).then(result => {
                    res.json(result)
                }).catch((e=>{
                    res.status(404).send(e)
                }));
        }catch (ex) {
            config.Logger(ex);
            res.send(ex)
        }
    }

}