"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const crypto = require("crypto");
exports.db = {
    user: 'sa',
    password: `sa1234`,
    server: 'localhost',
    database: 'DB_BKF',
    connectionTimeout: 5000,
    options: { encrypt: true, appName: 'BKF' }
};
function Logger(message) {
    try {
        let date = new Date();
        let filename = "app_log/SystemOut_" + date.toLocaleDateString() + ".log";
        var stream = fs.createWriteStream(filename, { flags: 'a' });
        stream.write("Info:: " + JSON.stringify(message) + "\n");
        stream.end();
    }
    catch (ex) {
        console.log(ex);
        throw ex;
    }
}
exports.Logger = Logger;
function getParametor(val) {
    let param = [];
    let prop;
    for (let item in val) {
        prop = new parameters();
        prop.key = item;
        prop.val = val[item];
        param.push(prop);
    }
    return param;
}
exports.getParametor = getParametor;
class parameters {
}
function Base64ToDisk(base64Image, fileSave) {
    return __awaiter(this, void 0, void 0, function* () {
        function decodeBase64Image(dataString) {
            var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
            var response = {};
            if (matches.length !== 3) {
                return new Error('Invalid input string');
            }
            response.type = matches[1];
            response.data = new Buffer(matches[2], 'base64');
            return response;
        }
        var imageTypeRegularExpression = /\/(.*?)$/;
        var seed = crypto.randomBytes(20);
        var uniqueSHA1String = crypto.createHash('sha1').update(seed).digest('hex');
        var base64Data = base64Image;
        var imageBuffer = yield decodeBase64Image(base64Data);
        var userUploadedFeedMessagesLocation = 'uploads/';
        var uniqueRandomImageName = 'image-' + uniqueSHA1String;
        var imageTypeDetected = imageBuffer.type.match(imageTypeRegularExpression);
        var userUploadedImagePath = userUploadedFeedMessagesLocation +
            uniqueRandomImageName +
            '.' +
            imageTypeDetected[1];
        try {
            fs.writeFile(userUploadedImagePath, imageBuffer.data, () => {
                console.log('DEBUG - feed:message: Saved to disk image attached by user:', userUploadedImagePath);
            });
            return userUploadedImagePath;
        }
        catch (error) {
            this.Logger(error);
            throw error;
        }
    });
}
exports.Base64ToDisk = Base64ToDisk;
function base64Encode(file) {
    try {
        var body = fs.readFileSync(file);
        return body.toString('base64');
    }
    catch (error) {
        this.Logger(error);
        throw error;
    }
}
exports.base64Encode = base64Encode;
